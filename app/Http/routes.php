<?php

use Illuminate\Http\Request;
use Develpr\AlexaApp\Facades\AlexaRouter;

AlexaRouter::launch('/alexa', 'App\Http\Controllers\API\AlexaController@alexalaunch');
AlexaRouter::intent('/alexa', 'GetColorIntent', 'App\Http\Controllers\API\AlexaController@alexaGetColor');
AlexaRouter::intent('/alexa', 'TellColorIntent', 'App\Http\Controllers\API\AlexaController@alexaTellColor');
AlexaRoute::sessionEnded('/alexa', function () {
    return '{"version":"1.0","response":{"shouldEndSession":true}}';
});