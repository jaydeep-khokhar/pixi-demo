<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use Develpr\AlexaApp\Request\AlexaRequest;
use Develpr\AlexaApp\Facades\Alexa;
use Develpr\AlexaApp\Response\AlexaResponse;

class AlexaController extends Controller
{

    /**
     * Alexa launch request.
     * @var alexa request data
     * @return json
     */
    function alexalaunch(AlexaRequest $alexarequest)
    {
        return Alexa::ask("Hello, welcome to support center. You can ask say my color is red")
            ->endSession('false');
    }
    /**
     * Alexa intent request.
     * @var alexa request data
     * @return json
     */
    function alexaGetColor(AlexaRequest $alexarequest)
    {
        $color = $alexarequest->slot("color");
        $session = Alexa::session('color', $color);
        return Alexa::say("Great, your color is " . $color . ". Now you can ask me what's my color.")->endSession('false');
    }

    /**
     * Alexa intent request.
     * @var alexa request data
     * @return json
     */
    function alexaTellColor()
    {
        $color = Alexa::session('color');
        return Alexa::say('I remember your color is ' . $color . ". Good Bye.")->endSession('true');
    }

    /**
     * Alexa termination intent request.
     * @var alexa request data
     * @return json
     */
    function alexaTerminate(AlexaRequest $alexarequest)
    {
        return Alexa::ask("Goodbye, have a nice day.")->endSession();
    }
}